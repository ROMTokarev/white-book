<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Models\PostModel;

class PostController extends Controller
{
    /**
     * Create a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['add', 'get']]);
    }

    public function add()
    {
        $data = Array();

        $data["post"] = request('text');

        if(auth()->user()) {
            $data["name"] = auth()->user()->name;
            $data["uid"]   = auth()->user()->id;
        } else {
            $data["name"] = "Incognito";
            $data["uid"]   = 0;
        }

        $data['created_at'] = new \DateTime;
        $data['updated_at'] = $data['created_at'];

        Mail::send('email.notification', ['data' => $data], function ($message) use ($data) {
            $message->from(env("MAIL_FROM_ADDRESS"), env("MAIL_FROM_NAME"));
            $message->to(env("MAIL_NOTIFICATION"), env("MAIL_NOTIFICATION"))->subject('Detect new post!');
        });

        return response()->json(PostModel::add($data));
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit()
    {
        $data         = request(['id', 'text']);
        $data["uid"]  = auth()->user()->id;
        $data["role"] = auth()->user()->role;
        $data['updated_at'] = new \DateTime;

        return response()->json(PostModel::edit($data));
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove()
    {
        $data = Array();

        $data["id"]   = request('id');
        $data["uid"]  = auth()->user()->id;
        $data["role"] = auth()->user()->role;

        return response()->json(PostModel::remove($data));
    }

    public function get()
    {
        return response()->json(PostModel::get());
    }
}

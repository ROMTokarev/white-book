<?php

namespace App\Http\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PostModel extends Model
{
    public static function add($data)
    {
        DB::table('post')->insert($data);
        
        $post = DB::table('post')->orderBy('created_at','desc')->get();
        return $post;
    }

    public static function edit($data)
    {
        if($data["role"] == "admin") {
            DB::table('post')
                ->where('id', $data["id"])
                ->update(['post' => $data["text"], 'updated_at' => $data["updated_at"]]);
        } else {
            DB::table('post')
                ->where('id', '=', $data["id"])
                ->where('uid', '=', $data["uid"])
                ->update(['post' => $data["text"], 'updated_at' => $data["updated_at"]]);
        }

        $post = DB::table('post')->orderBy('created_at','desc')->get();
        return $post;

    }

    public static function remove($data)
    {
        if($data["role"] == "admin") {
            DB::table('post')->where('id', '=', $data['id'])->delete();
        } else {
            DB::table('post')
                ->where('id', '=', $data['id'])
                ->where('uid', '=', $data["uid"])->delete();
        }
        
        $post = DB::table('post')->orderBy('created_at','desc')->get();
        return $post;
    }

    public static function get()
    {
        $post = DB::table('post')->orderBy('created_at','desc')->get();
        return $post;
    }
}

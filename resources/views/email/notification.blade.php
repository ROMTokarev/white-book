
@extends('email.child')

@section('content')

 <center style="background-color:#edeff2;">
      
      <table style="max-width:100% !important;width:600px !important;min-width:600px !important;" class="mceItemTable" cellspacing="0" cellpadding="0" border="0" bgcolor="#edeff2" width="600">
        <tbody>
          <tr>
            <td colspan="2" style="vertical-align:bottom;" bgcolor="#edeff2" align="right" width="100%" height="45">

            </td>
          </tr>
        </tbody>
      </table>
      
      
      <table style="background-color:#fcfcfc;width:600px;" class="mceItemTable" cellspacing="0" cellpadding="0" border="0" bgcolor="#fcfcfc" width="600">
        <tbody>
          
          
          <tr>
            <td colspan="2" style="background-color:#fcfcfc;padding-left:40px;padding-right:40px;padding-top:15px;padding-bottom:0;" bgcolor="#fcfcfc" width="100%">
              <div style="font-family:Arial, Helvetica, sans-serif;">

                <h3>Nontificaion from admin White Book. Detect new post!</h3>


                <table style="background-color:#fcfcfc;width:100%;" class="mceItemTable" cellspacing="0" cellpadding="0" border="0" bgcolor="#fcfcfc" width="100%">
                  <tbody><tr>
                    <td style="background-color:#fff;padding-left:20px;padding-right:20px;padding-top:20px;padding-bottom:40px;" bgcolor="#ffffff" width="100%">
                      <div style="font-family:Arial, Helvetica, sans-serif;">
                        <span style="font-size:14px;line-height:21px;padding:0;margin:0;">

                        <p>Sender name: {{ $data["name"] }} </p>
                        <p>Data: {{ date_format($data["created_at"], 'Y-m-d H:i:s') }} </p>
                        <p>Message text: {{ $data["post"] }} </p>

                        </span>

                      </div>
                    </td>
                  </tr>
                </tbody></table>
              </div>
            </td>
          </tr>
        </tbody>
      </table>

<div style="background-color:#fcfcfc;width:600px;"><br/></div>

 <table style="background-color:#fcfcfc;width:600px;" class="mceItemTable" cellspacing="0" cellpadding="0" border="0" bgcolor="#fcfcfc" width="600">
        <tbody><tr>
          <td style="background-color:#fcfcfc;padding-left:0;padding-right:0;padding-top:0;padding-bottom:0;" width="160">

          </td>
        <td style="background-color:#fcfcfc;padding-left:0;padding-right:0;padding-top:0;padding-bottom:0;margin-bottom:50px;" width="160">

        </td>
      </tr>
      <tr>
        <td><br/><br/></td>
         </tbody>
      </table>
@endsection

import './bootstrap';
import App from './react';
import React from 'react';
import thunk from 'redux-thunk';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import rootReducer from './react/store/reducers'
import { createStore, applyMiddleware } from 'redux'

const store = createStore(rootReducer, applyMiddleware(thunk))

ReactDOM.render(<Provider store={store}>
  <App />
</Provider>, document.getElementById('app'));



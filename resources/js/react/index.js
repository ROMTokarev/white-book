import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, NavLink, BrowserRouter as Router, Switch } from "react-router-dom";
import Login from "./containers/auth/login"
import Registration from "./containers/auth/registration"
import Post from "./containers/post"
import * as actions from "./store/actions"
import {withRouter} from 'react-router'
import * as access from './utils/access'

class App extends Component {

  constructor(props) {
    super(props);
    if(access.getUser() != null) {
      this.props.dispatch(actions.user(access.getUser()))
    }
  }

  logout() {
    this.props.dispatch(actions.user(null))
    access.delUser()
  }

  render() {
    let token = ""
    const user = this.props.user
    if(user !== null){
        token = user.original.access_token
    }
    return (
      <Router>
        <React.Fragment>
        <nav className="navbar navbar-expand-md navbar-dark bg-dark">
          <div className="collapse navbar-collapse">
            <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <NavLink className="nav-link" exact to="/">Post</NavLink>
                </li>
            </ul>
            <div id={token == "" ? "" : "authNoDisplay"}  className="mt-2 mt-md-0 margin">
              <NavLink className="btn btn-outline-warning my-2 my-sm-0" to="/login">Sign In</NavLink>
              <NavLink className="btn btn-outline-warning my-2 my-sm-0" to="/registration">Sign Up</NavLink>
            </div>
            <div id={token == "" ? "authNoDisplay" : ""}  className="mt-2 mt-md-0 margin">
              <span onClick={e => this.logout()} className="btn btn-outline-warning my-2 my-sm-0 pointer">Sign Out</span>
            </div>
          </div>
        </nav>
          <div className="content">
            <Switch>
              <Route exact path="/" component={Post} />
              <Route path="/login" component={Login} />
              <Route path="/registration" component={Registration} />
            </Switch>
          </div>
        </React.Fragment>
      </Router>

    )
  }
}

const mapStateToProps = state => ({
  user: state.data.user
})

export default connect(mapStateToProps)(App);

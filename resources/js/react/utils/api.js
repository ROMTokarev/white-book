import axios from 'axios';
import * as access from './access'
const qs = require('querystring')

export const auth = (data) => {
  return axios({
    method: 'post',
    url: '/api/auth/login',
    data: qs.stringify({
      email: data.email,
      password: data.password
    }),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    }
  })
}

export const registration = (data) => {
  return axios({
    method: 'post',
    url: '/api/auth/registration',
    data: qs.stringify({
      name: data.name,
      email: data.email,
      password: data.password
    }),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    }
  })
}

export const getPost = () => {
  return axios({
    method: 'get',
    url: '/api/post',
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    }
  })
}

export const  addPost = (data) => {
  let token = ""
  if(access.getUser() !== null){
    token = access.getUser().original.access_token
  }
  return axios({
    method: 'post',
    url: '/api/post',
    data: qs.stringify({
      text: data.text
    }),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json",
      "Authorization": `Bearer ${token}`
    }
  })
}

export const editPost = (data) => {
  let token = ""
  if(access.getUser() !== null){
    token = access.getUser().original.access_token
  }
  return axios({
    method: 'put',
    url: '/api/post',
    data: qs.stringify({
      id: data.id,
      text: data.text
    }),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json",
      "Authorization": `Bearer ${token}`
    }
  })
}

export const removePost = (data) => {
  let token = ""
  if(access.getUser() !== null){
    token = access.getUser().original.access_token
  }
  return axios({
    method: 'delete',
    url: '/api/post',
    data: qs.stringify({
      id: data.id,
    }),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Accept": "application/json",
      "Authorization": `Bearer ${token}`
    }
  })
}
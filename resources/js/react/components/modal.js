import React, { Component } from 'react';
import { Editor } from '@tinymce/tinymce-react';
import * as api from '../utils/api'
import * as actions from "../store/actions"
import { connect } from 'react-redux'

class Modal extends Component {
 
  constructor(props) {
    super(props);
    this.state = {
      plugins: "print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker",
      toolbar: "formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment",
      text: "",
      current: -1,
      id: -1
    }
  }

  handleEditorChange(content) {
    this.setState({text: content})
  }

  editPost() {
    api.editPost({id: this.state.id, text: this.state.text}).then(res => {
      this.props.dispatch(actions.post(res.data))
    })
  }

  render() {
    const edit = this.props.edit
    if(edit >= 0 && edit !== this.state.current) {
      const post = this.props.post[edit]
      this.setState({text: post.post, id: post.id, current: edit})
    }
    return (
        <React.Fragment>
         <div className="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">Edit comment</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                          <Editor value={this.state.text} onEditorChange={(content, editor) => this.handleEditorChange(content)} init={{ height: 400, plugins: this.state.plugins, toolbar: this.state.toolbar, image_advtab: true }}/>      
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="button" onClick={e => this.editPost()} className="btn btn-primary" data-dismiss="modal">Save changes</button>
                </div>
              </div>
          </div>
         </div>
        </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  edit: state.data.edit,
  post: state.data.post
})

export default connect(mapStateToProps)(Modal);
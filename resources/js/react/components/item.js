import React, { Component } from 'react';
import { Editor } from '@tinymce/tinymce-react';
import * as api from '../utils/api'
import * as actions from "../store/actions"
import { connect } from 'react-redux'

class Item extends Component {
 
  constructor(props) {
    super(props);

    api.getPost().then(res => {
      this.props.dispatch(actions.post(res.data))
    })
  }

  edit(element) {
   this.props.dispatch(actions.edit(element))
  }

  delete(id) {
   api.removePost({id: id}).then(res => {
      this.props.dispatch(actions.post(res.data))
   })
  }

  render() {
     const post = this.props.post
     let token = ""
     const user = this.props.user
     if(user !== null){
         token = user.original.access_token
     }
    return (
        <React.Fragment>
           {post ?
               post.map((item, i) => (
                  <div className="item-padding">
                     <div className="row comment">
                        <div className="col-md-12 header padding">
                           <div className="row">
                              <div className="col-md-6">Name: {item.name}</div>
                              <div className="col-md-6 text-right italic">Create at: {item.created_at}</div>
                           </div>
                        </div>
                        <div className="col-md-12 padding">
                           <div className="row">
                              <div className="col-md-12 message-padding" dangerouslySetInnerHTML={{ __html: item.post  }}>
                              </div>
                              <div className="col-md-6 padding">
                                 <span className="italic">
                                    Update at: {item.updated_at}
                                 </span>
                              </div>
                              <div id={token == "" || +user.user.id !== +item.uid && user.user.role !== "admin"  ? "authNoDisplay" : ""} className="col-md-6 text-right padding">
                                 <span className="padding-btn">
                                    <button type="button" onClick={e => this.edit(i)} className="btn btn-primary btn-sm"
                                                                                       data-toggle="modal" data-target="#exampleModal">Edit</button>
                                 </span>
                                 <span className="padding-btn">
                                    <button onClick={e => this.delete(item.id)} type="button" className="btn btn-danger btn-sm">Delete</button>
                                 </span>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               )) : <div></div>}
        </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
   post: state.data.post,
   user: state.data.user
})

export default connect(mapStateToProps)(Item);
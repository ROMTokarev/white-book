import React, { Component } from 'react'
import { Editor } from '@tinymce/tinymce-react'
import * as api from '../utils/api'
import * as actions from "../store/actions"
import { connect } from 'react-redux'

class Wysiwyg extends Component {
 
  constructor(props) {
    super(props);
    this.state = {
      plugins: "print preview fullpage powerpaste searchreplace autolink directionality advcode visualblocks visualchars fullscreen image link media mediaembed template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount tinymcespellchecker a11ychecker imagetools textpattern help formatpainter permanentpen pageembed tinycomments mentions linkchecker",
      toolbar: "formatselect | bold italic strikethrough forecolor backcolor permanentpen formatpainter | link image media pageembed | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | removeformat | addcomment",
      text: ""
    }
  }

  handleEditorChange(content) {
    this.setState({text: content})
  }

  addPost() {
    api.addPost({text: this.state.text}).then(res => {
      this.props.dispatch(actions.post(res.data))
    })
  }

  render() {
    return (
        <React.Fragment>
          <div className="row justify-content-center">
            <div className="col-md-12 nopading">
                <Editor value={this.state.text} onEditorChange={(content, editor) => this.handleEditorChange(content)} init={{ height: 400, plugins: this.state.plugins, toolbar: this.state.toolbar, image_advtab: true }} />
                <button type="button" onClick={e => this.addPost()} className="btn btn-success btn-sm right">Add</button>
            </div>
          </div>
        </React.Fragment>
    )
  }
}

export default connect()(Wysiwyg);
import React, { Component } from 'react';
import Wysiwyg from "../components/wysiwyg"
import Item from "../components/item"
import Modal from "../components/modal"

class Post extends Component {
  render() {
    return (
      <div>
        <Modal/>
        <div className="row justify-content-center">
          <div className="col-md-8">
            <br/>
              <Item/>
            <br/>
              <Wysiwyg/>
            <br/>
          </div>
        </div>
        </div>
    )
  }
}

export default Post;
import React, { Component } from 'react';
import * as api from "../../utils/api"
import { connect } from 'react-redux'
import * as  actions from '../../store/actions'
import {withRouter} from 'react-router'
import * as access from '../../utils/access'

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = ({
      email: "",
      password: ""
    })
  }

  emailChange(event) {
    this.setState({email: event.target.value})
  }
  
  passwordChange(event) {
    this.setState({password: event.target.value})
  }
  
  auth() {
    api.auth({email: this.state.email, password: this.state.password}).then(res => {
      this.props.dispatch(actions.user(res.data))
      access.setUser(res.data)
      this.props.history.push('/');
    })
  }
 
  render() {
    return (
      <div className="sign text-center">
        <form className="form-signin">
          <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
          <label for="inputEmail" className="sr-only">Email address</label>
          <input type="email" id="inputEmail" value={this.state.email}  onChange={(e) => this.emailChange(e)} className="form-control" placeholder="Email address" required />
          <label for="inputPassword" className="sr-only">Password</label>
          <input type="password" value={this.state.password}  onChange={(e) => this.passwordChange(e)} id="inputPassword" className="form-control" placeholder="Password" required />
          <button onClick={e => this.auth()} className="btn btn-lg btn-primary btn-block" type="button">Sign in</button>
        </form>
      </div>
    )
  }
}

export default connect()(Login);

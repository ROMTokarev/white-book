import React, { Component } from 'react';
import {withRouter} from 'react-router'
import * as api from "../../utils/api"

class Registration extends Component {

  constructor(props) {
    super(props);
    this.state = ({
      name: "",
      email: "",
      password: ""
    })
  }

  nameChange(event) {
    this.setState({name: event.target.value})
  }

  emailChange(event) {
    this.setState({email: event.target.value})
  }
  
  passwordChange(event) {
    this.setState({password: event.target.value})
  }

  registration() {
    api.registration({name: this.state.name, email: this.state.email, password: this.state.password}).then(res => {
      this.props.history.push('/login');
    })
  }

  render() {
    return (
      <div className="sign text-center">
        <form className="form-signin">
          <h1 className="h3 mb-3 font-weight-normal">Please sign up</h1>
          <label for="inputName" className="sr-only">Name</label>
          <input type="text" value={this.state.name}  onChange={(e) => this.nameChange(e)} id="inputName" className="form-control" placeholder="Name" required />
          <label for="inputEmail" className="sr-only">Email address</label>
          <input type="email" value={this.state.email}  onChange={(e) => this.emailChange(e)} id="inputEmail" className="form-control" placeholder="Email address" required />
          <label for="inputPassword" className="sr-only">Password</label>
          <input type="password" value={this.state.password}  onChange={(e) => this.passwordChange(e)} id="inputPassword" className="form-control" placeholder="Password" required />
          <button onClick={e => this.registration()} className="btn btn-lg btn-primary btn-block" type="button">Sign up</button>
        </form>
      </div>
    )
  }
}

export default Registration;
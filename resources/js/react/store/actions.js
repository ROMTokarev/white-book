export const reset = () => {
  return function action (dispatch) {
    dispatch( ({ type: 'RESET' }) )
    return "RESET"
  }
}

export const user = (user) => {
  return function action (dispatch) {
      dispatch( ({ type: 'user', user }) )
      return "user"
  }
}

export const post = (post) => {
  return function action (dispatch) {
      dispatch( ({ type: 'post', post }) )
      return "post"
  }
}

export const edit = (edit) => {
  return function action (dispatch) {
      dispatch( ({ type: 'edit', edit }) )
      return "edit"
  }
}


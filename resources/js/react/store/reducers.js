import { combineReducers } from 'redux'

const INITIAL_STATE = {
  user: null,
  post: "",
  edit: -1
}

export const data = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'user':
      return {
        ...state,
        user: action.user
      }
    case 'post':
      return {
        ...state,
        post: action.post
      }
    case 'edit':
      return {
        ...state,
        edit: action.edit
      }
    case "RESET":
      return INITIAL_STATE;
    default:
      return state
  }
}


export default combineReducers({
  data
})

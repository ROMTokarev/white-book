<h3>Запуск dev сервера:</h3>
<ul>
    <li>composer install</li>
    <li>npm install</li>
    <li>cp .env.dist .env</li>
    <li>touch /var/www/whiteBook.sqlite</li>
    <li>php artisan key:generate</li>
    <li>php artisan jwt:secret</li>
    <li>php artisan migrate:refresh --seed</li>
    <li>npm run dev</li>
    <li>php artisan serve</li>
</ul>
<h3>Демо пользователь (админ):</h3>
<ul>
    <li>Login: admin@demo.ru</li>
    <li>Pass: admin</li>
</ul>
<h3>Если enveroment попал в кэш:</h3>
<ul>
    <li>php artisan config:cache</li>
    <li>php artisan config:clear</li>
</ul>
